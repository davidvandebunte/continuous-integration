#!/bin/sh

readonly OCI_REPOSITORY=$1
readonly BAZEL_VERSION=$2

set -o errexit -o nounset

print_usage() {
    >&2 echo "Usage: $0 <OCI_REPOSITORY> <BAZEL_VERSION>"
}

if [ -z "${OCI_REPOSITORY}" ]; then
    >&2 echo "ERROR: missing 'OCI_REPOSITORY' argument"
    print_usage
    exit 1
fi

if [ -z "${BAZEL_VERSION}" ]; then
    >&2 echo "ERROR: missing 'BAZEL_VERSION' argument"
    print_usage
    exit 1
fi

GIT_ROOT=$(git rev-parse --show-toplevel)
readonly GIT_ROOT

# https://stackoverflow.com/questions/18012930
if docker buildx version 2>/dev/null 1>&2; then
    buildx="buildx"
fi

# https://github.com/docker/buildx/issues/132
DOCKER_BUILDKIT=1 docker ${buildx:+"${buildx}"} build \
    --file "${GIT_ROOT}/bazel/oci/Dockerfile" \
    --tag "${OCI_REPOSITORY}:${BAZEL_VERSION}" \
    --build-arg BAZEL_VERSION="${BAZEL_VERSION}" \
    "${GIT_ROOT}"
